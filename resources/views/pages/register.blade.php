<!DOCTYPE html>
<html>
    <head>
        <h1>Buat Account Baru!</h1>
    </head>

    <body>
        <h2>Sign Up Form</h2>
        <form action="/submit" method="post">
            @csrf
            <label for="fname">First name:</label><br>
            <input type="text" id="fname" name="fname"><br>
            
            <label for="lname">Last name:</label><br>
            <input type="text" id="lname" name="lname"><br><br>
            
            <label for="gender">Gender:</label><br>
            <input type="radio" id="male" name="gender" value="Male">
            <label for="male">Male</label>
            <input type="radio" id="female" name="gender" value="Female">
            <label for="female">Female</label>
            <input type="radio" id="other" name="gender" value="Other">
            <label for="other">Other</label>
            <br><br>

            <label for="nationality">Nationality:</label><br>
            <select name="nationality" id="nationality">
                <option value="Indonesian">Indonesian</option>
                <option value="Malaysian">Malaysian</option>
                <option value="Singaporean">Singaporean</option>
                <option value="Others">Others</option>
            </select>
            <br><br>

            <label for="language">Language Spoken:</label><br>
            <input type="checkbox" name="lang_indo" id="bhs_indo">
            <label for="language1">Bahasa Indonesia</label><br>
            <input type="checkbox" name="lang_english" id="lang_english">
            <label for="language2">English</label><br>
            <input type="checkbox" name="lang_other" id="lang_other">
            <label for="language3">Other</label><br><br>        
        
            <label for="bio">Bio:</label><br>
            <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
            <br><br>
            
          <input type="submit" value="Submit">
        </form>
    </body>
</html>